# SubMatrix

SubMatrix is a project to collect information sources and tools for the collection or discovery of subdomains.

The aim of this project is to group this collected information and to be able to group and classify each of the subdomain enumeration tools as well as the references from which they extract the information.

This project is based on the idea of [C2Matrix](https://docs.google.com/spreadsheets/d/1b4mUxa6cDQuTV2BPC6aA-GR4zGZi0ooPYtBe4IgPsSc/edit#gid=0).

## Matrix

| API/TOOLS      |        Amass       |     Findomain      |      Sub.sh        |     AssetFinder    |     Subfinder      |         Gau        |     Sublist3r      |
|---------------:|:------------------:|:------------------:|:------------------:|:------------------:|:------------------:|:------------------:|:------------------:|
| AlientVault    | :heavy_check_mark: |                    | :heavy_check_mark: |                    | :heavy_check_mark: | :heavy_check_mark: |                    |
| Anubis         | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |                    | :heavy_check_mark: |                    |                    |
| Archiveis      |                    |                    |                    |                    | :heavy_check_mark: |                    |                    |
| Archiveorg     |                    | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |                    |
| Ask            |                    |                    |                    |                    |                    |                    | :heavy_check_mark: |
| Baidu          |                    |                    |                    |                    |                    |                    | :heavy_check_mark: |
| BinaryEdge     | :heavy_check_mark: |                    | :heavy_check_mark: |                    | :heavy_check_mark: |                    |                    |
| Bing           |                    |                    |                    |                    |                    |                    | :heavy_check_mark: |
| BGPView        | :heavy_check_mark: |                    | :heavy_check_mark: |                    |                    |                    |                    |
| BufferOver     | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |                    |                    |
| C99            | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |                    |                    |                    |                    |
| Censys         |                    |                    |                    |                    | :heavy_check_mark: |                    |                    |
| Certspotter    |                    | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |                    |                    |
| Chaos          |                    |                    |                    |                    | :heavy_check_mark: |                    |                    |
| Cloudflare     | :heavy_check_mark: |                    | :heavy_check_mark: |                    |                    |                    |                    |
| CommonCrawl    | :heavy_check_mark: |                    | :heavy_check_mark: |                    | :heavy_check_mark: | :heavy_check_mark: |                    |
| Crtsh          |                    | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |                    | :heavy_check_mark: |
| Ctsearch       |                    | :heavy_check_mark: | :heavy_check_mark: |                    |                    |                    |                    |
| Dnsdumpster    |                    |                    | :heavy_check_mark: |                    | :heavy_check_mark: |                    | :heavy_check_mark: |
| DNSDB          | :heavy_check_mark: |                    | :heavy_check_mark: |                    | :heavy_check_mark: |                    |                    |
| Facebook       |                    |                    |                    |                    |                    |                    | :heavy_check_mark: |
| Google         |                    |                    |                    |                    |                    |                    | :heavy_check_mark: |
| GitHub         | :heavy_check_mark: |                    | :heavy_check_mark: |                    | :heavy_check_mark: |                    |                    |
| HackerTarget   | :heavy_check_mark: |                    | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |                    |                    |
| Intelx         |                    |                    |                    |                    | :heavy_check_mark: |                    |                    |
| Mnemonic       | :heavy_check_mark: |                    | :heavy_check_mark: |                    |                    |                    |                    |
| NetworksDB     | :heavy_check_mark: |                    | :heavy_check_mark: |                    |                    |                    |                    |
| PassiveTotal   | :heavy_check_mark: |                    | :heavy_check_mark: |                    | :heavy_check_mark: |                    |                    |
| Pastebin       | :heavy_check_mark: |                    | :heavy_check_mark: |                    |                    |                    |                    |
| RADb           | :heavy_check_mark: |                    | :heavy_check_mark: |                    |                    |                    |                    |
| Rapiddns       |                    |                    | :heavy_check_mark: |                    | :heavy_check_mark: |                    |                    |
| ReconDev       | :heavy_check_mark: |                    | :heavy_check_mark: |                    | :heavy_check_mark: |                    |                    |
| Riddler        |                    |                    | :heavy_check_mark: |                    | :heavy_check_mark: |                    |                    |
| Robtex         | :heavy_check_mark: |                    | :heavy_check_mark: |                    | :heavy_check_mark: |                    |                    |
| SecurityTrails | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |                    | :heavy_check_mark: |                    |                    |
| ShadowServer   | :heavy_check_mark: |                    | :heavy_check_mark: |                    |                    |                    |                    |
| Shodan         | :heavy_check_mark: |                    | :heavy_check_mark: |                    | :heavy_check_mark: |                    |                    |
| Sitedossier    |                    |                    |                    |                    | :heavy_check_mark: |                    |                    |
| SonarSearch    | :heavy_check_mark: |                    | :heavy_check_mark: |                    | :heavy_check_mark: |                    |                    |
| Spyse          | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |                    |                    |
| Sublist3rAPI   | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |                    | :heavy_check_mark: |                    | :heavy_check_mark: |
| TeamCymru      | :heavy_check_mark: |                    | :heavy_check_mark: |                    |                    |                    |                    |
| ThreatBook     | :heavy_check_mark: |                    | :heavy_check_mark: |                    | :heavy_check_mark: |                    |                    |
| ThreatCrowd    | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |                    | :heavy_check_mark: |
| ThreatMiner    | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |                    | :heavy_check_mark: |                    |                    |
| Twitter        | :heavy_check_mark: |                    | :heavy_check_mark: |                    |                    |                    |                    |
| URLScan        | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |                    |                    |                    |                    |
| VirusTotal     | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |                    | :heavy_check_mark: |
| WhoisXML       | :heavy_check_mark: |                    | :heavy_check_mark: |                    |                    |                    |                    |
| Yahoo          |                    |                    |                    |                    |                    |                    | :heavy_check_mark: |
| ZETAlytics     | :heavy_check_mark: |                    | :heavy_check_mark: |                    |                    |                    |                    |
| ZoomEye        | :heavy_check_mark: |                    | :heavy_check_mark: |                    | :heavy_check_mark: |                    |                    |


## Tools

* [**Amass**](https://github.com/OWASP/Amass)
* [**Findomain**](https://github.com/Findomain/Findomain)
* [**Sub.sh**](https://github.com/cihanmehmet/sub.sh)
* [**AssetFinder**](https://github.com/tomnomnom/assetfinder)
* [**Subfinder**](https://github.com/projectdiscovery/subfinder)
* [**Gau**](https://github.com/lc/gau)
* [**Sublist3r**](https://github.com/aboul3la/Sublist3r)

## APIs

* [**AlientVault**](https://otx.alienvault.com)
* [**Anubis**](https://jldc.me/anubis)
* [**Archiveis**](http://archive.is)
* [**Archiveorg**](http://web.archive.org)
* [**BinaryEdge**](https://www.binaryedge.io)
* [**BGPView**](https://bgpview.docs.apiary.io)
* [**BufferOver**](https://dns.bufferover.run)
* [**C99**](https://api.c99.nl)
* [**Censys**](https://censys.io)
* [**Certspotter**](https://certspotter.com)
* [**Chaos**](https://chaos.projectdiscovery.io)
* [**Cloudflare**](https://api.cloudflare.com)
* [**CommonCrawl**](https://index.commoncrawl.org)
* [**Crtsh**](https://crt.sh)
* [**Ctsearch**](https://transparencyreport.google.com/https/certificates)
* [**Dnsdumpster**](https://dnsdumpster.com)
* [**DNSDB**](https://docs.dnsdb.info)
* [**GitHub**](https://docs.github.com/en/rest)
* [**HackerTarget**](https://api.hackertarget.com)
* [**Intelx**](https://intelx.io)
* [**Mnemonic**](https://docs.mnemonic.no)
* [**NetworksDB**](https://networksdb.io/api/docs)
* [**PassiveTotal**](https://api.passivetotal.org)
* [**Pastebin**](https://pastebin.com/doc_api)
* [**RADb**](https://www.radb.net)
* [**Rapiddns**](https://rapiddns.io)
* [**ReconDev**](https://recon.dev)
* [**Riddler**](https://riddler.io/help/api)
* [**Robtex**](https://www.robtex.com/api)
* [**SecurityTrails**](https://securitytrails.com/corp/api)
* [**ShadowServer**](https://www.shadowserver.org)
* [**Shodan**](https://developer.shodan.io)
* [**Sitedossier**](http://www.sitedossier.com)
* [**SonarSearch**](https://sonar.omnisint.io)
* [**Spyse**](https://spyse.com)
* [**Sublist3rAPI**](https://api.sublist3r.com)
* [**TeamCymru**](https://team-cymru.com)
* [**ThreatBook**](https://pypi.org/project/threatbook-API)
* [**ThreatCrowd**](https://www.threatcrowd.org)
* [**ThreatMiner**](https://api.threatminer.org)
* [**Twitter**](https://developer.twitter.com)
* [**URLScan**](https://urlscan.io/docs/api)
* [**VirusTotal**](https://www.virustotal.com)
* [**WhoisXML**](https://www.whoisxmlapi.com)
* [**ZETAlytics**](https://zetalytics.com)
* [**ZoomEye**](https://www.zoomeye.org/doc)
